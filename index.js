require('dotenv').config();
const mongoose= require('mongoose');
const express = require('express');
require('./db/conn');
const products=require('./model/productsSchema');
const goldprice=require('./model/goldpriceSchema');
const solitairprice=require('./model/solitarepriceSchema');
const app = express();
 
app.use(express.json());


app.get('/contact',(req,res)=>{
    res.send(`hellow contact world from the server`);
});
// app.post('/add-product',(req,res)=>{
//     console.log(req.body)
//     const pro= new products(req.body)
//     pro.save().then(()=>{
//         res.status(201).send(pro)
//     }).catch((e)=>{
//         console.log(e)
//         res.send(e);
//     })
// });
app.post('/add-product',async(req,res)=>{
    try{
        const pro= new products(req.body);
        const createproduct= await pro.save();
        res.status(201).send(createproduct);
    }catch(e){
        res.status(400).send(e);
    }
});
app.post('/add-goldprice',async(req,res)=>{
    try{
        const gprice= new goldprice(req.body);
        const creategprice= await gprice.save();
        res.status(201).send(creategprice);
    }catch(e){
        res.status(400).send(e);
    }
});
app.post('/add-solitaireprice',async(req,res)=>{
    try{
        const solitairPrice= new solitairprice(req.body);
        const createsolitairprice= await solitairPrice.save();
        res.status(201).send(createsolitairprice);
    }catch(e){
        res.status(400).send(e);
    }
});
// app.get('/fetch-product',async(req,res)=>{
//     try{
//         const fetchpro= await products.aggregate([
//             {
//                 $lookup:{
//                     from : 'goldprices',
//                     localField : 'goldKarat',
//                     foreinField : 'goldKarat',
//                     as : 'goldprice',
//                 }
//             }
//         ]).toArray((err,res)=>{
//             if(err) throw err;
//             console.log(JSON.stringify(res));

//         });
       
//         res.send(fetchpro);
//     }catch(e){
//         res.send(e);
//     }
// });
app.get('/fetch-product',(req,res)=>{
    try{
        const fetchpro= products.aggregate([
     {
                $lookup:{
                    from : 'goldprices',
                    localField : 'goldKarat',
                    foreignField : 'goldKarat',
                    as : 'goldrate',
                },

            },
            {$unwind: '$goldrate'},
            {
                $lookup:{
                    from : 'solitareprices',
                    localField : 'goldKarat',
                    foreignField : 'goldKarat',
                    as : 'solitairerate',
                }
            },
            {$unwind: '$solitairerate'},
            {$addFields:{
                product_price:{ 
                    $add:[{$multiply: [ "$goldrate.price","$goldWeight"]},{$multiply: [ "$solitairerate.price","$SolitareWeight"]},{$divide:[{$multiply:[ "$goldrate.price",10]},100]}]
                    // {$divide:[[{$multiply:[ "$goldrate.price",10],100]
  }
            }}
            ,{ $project:{
                goldWeight:1,
                SolitareWeight:1,
                goldKarat:1,
                product_price:1
            }
            },
            { $sort : { product_price : -1 } }
        ]).exec(function(err, results){
            console.log(results)
                         // formula (goldWeight * goldPrice) + (SolitareWeight * SolitarePrice) + 10% Making charges of gold Price = Product Price
            // for(let i=0;i<results.length;i++){
            //     console.log(results[i].goldrate[0].price);
            //     let tenpercent=(results[i].goldrate[0].price*10)/100;
            //     let products_price=(results[i].goldWeight*results[i].goldrate[0].price)+(results[i].SolitareWeight*results[i].solitairerate[0].price)+tenpercent;

            //     results[i].product_price=products_price;
            // }  
            //for shorting on behalf product value desc
            // const eitherSort = (results = []) => {
            //     const sorter = (a, b) => {
            //        return +b.product_price - +a.product_price;
            //     };
            //     results.sort(sorter);
            //  };
            //  eitherSort(results);
            res.send(results);
         });     
    }catch(e){
        console.log(e);
        res.send(e);
    }
});

app.listen(3000, () => {
    console.log(`Server Started at ${3000}`)
})