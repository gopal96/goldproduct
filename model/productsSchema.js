
const mongoose= require('mongoose');


const productsSchema = new mongoose.Schema({
    title:{
        type: String,
        required: true
    },
    goldWeight:{
        type:Number,
        required:true
    },
    SolitareWeight:{
        type:Number,
        required:true
    },
    goldKarat:{
        type:Number,
        required:true,
        ref:"goldprices"
    },
    daimondQality:{
        type:String,
        required:true
  
    }

})




const Products = mongoose.model('PRODUCTS',productsSchema);
module.exports=Products;

